export interface UserExistsRequest {
    email?: string;
    mobile?: string;
}