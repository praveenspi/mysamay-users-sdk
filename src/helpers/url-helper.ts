import { ContextVariable } from "@neb-sports/mysamay-common-utils";

export class URLHelper {
    baseurl: string;
    contextPath: string = "users-srv";
    checkUserExists: string = this.contextPath + "/users/exists";
    usersByIdList: string = this.contextPath + "/users/byId";
    userById: string = this.contextPath + "/users";
    searchUser: string = this.contextPath + "/users/search";
    userProfile: string = this.contextPath + "/profile";
    getPassword: string = this.contextPath + "password/user"
    getUsersCount: string = this.contextPath + "/users/count"
    getParticipantsCount: string = this.contextPath + "/users/event/count"

    constructor(contextVar: ContextVariable) {
        this.baseurl = contextVar.baseUrl;
    }

}