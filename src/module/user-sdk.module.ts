import { UsersSDK } from './../providers/user-sdk';
import { Module } from '@nestjs/common';

@Module({
    imports: [],
    providers: [
      UsersSDK
    ],
    exports: [
      UsersSDK
    ]
  })
export class UserSDKModule {}
