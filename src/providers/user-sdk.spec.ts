import { ResponseEntity } from '@neb-sports/mysamay-common-utils';
import { HttpHelper } from '@neb-sports/mysamay-common-utils';
import { ContextVariable } from '@neb-sports/mysamay-common-utils';
import { MetadataHelper } from '@neb-sports/mysamay-common-utils';
import { UsersSDK } from './user-sdk';
import { Test, TestingModule } from '@nestjs/testing';



describe('UsersSDK', () => {
    let sdk: UsersSDK;


    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [],
            providers: [UsersSDK],
        }).compile();

        sdk = module.get<UsersSDK>(UsersSDK);
    });

    it('should be defined', () => {
        expect(sdk).toBeDefined();
    });

    it('should check user exists', async () => {
        let contextVar: ContextVariable = {
            baseUrl: "http://localhost",
            refNumber: "1234"
        }
        let resp = {
            body: {
                success: true,
                status: 200,
                data: true
            }
        }
        MetadataHelper.getContextVariable = jest.fn().mockReturnValue(contextVar);
        HttpHelper.postRequest = jest.fn().mockResolvedValue(resp);
        let result = await sdk.checkUserExists({ email: "test@gmail.com" });
        expect(result).toBe(resp.body);
    });

    it('should get user by Id List', async () => {
        let contextVar: ContextVariable = {
            baseUrl: "http://localhost",
            refNumber: "1234"
        }
        let resp = {
            body: {
                success: true,
                status: 200,
                data: [
                    {email: "text@gmail.com"}
                ]
            }
        }
        MetadataHelper.getContextVariable = jest.fn().mockReturnValue(contextVar);
        HttpHelper.postRequest = jest.fn().mockResolvedValue(resp);
        let result = await sdk.getUsersByIdList(["1234"]);
        expect(result).toBe(resp.body);
    });

});
