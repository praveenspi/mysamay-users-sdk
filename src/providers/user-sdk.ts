import { URLHelper } from './../helpers/url-helper';
import { Injectable } from "@nestjs/common";

import urljoin from "url-join";
import { HttpHelper, MetadataHelper, ResponseEntity } from "@neb-sports/mysamay-common-utils";
import { PasswordEntity, UserEntity } from "@neb-sports/mysamay-users-model";
import { UserExistsRequest } from "../entities/requests/user-exists-request";
import { FilterQuery } from 'mongodb';

@Injectable()
export class UsersSDK {

    urlHelper: URLHelper;

    constructor() {

    }

    init() {
        this.urlHelper = new URLHelper(MetadataHelper.getContextVariable());
    }

    async getPassword(userId: string): Promise<ResponseEntity<PasswordEntity>> {
        try {
            this.init();
            let url = urljoin(this.urlHelper.baseurl, this.urlHelper.getPassword, userId);
            let headers = this.prepareHeader();
            let resp = await HttpHelper.getRequest<ResponseEntity<PasswordEntity>>(url, headers);
            return resp.body;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async getUserProfile(): Promise<ResponseEntity<UserEntity>> {
        try {
            this.init();
            let url = urljoin(this.urlHelper.baseurl, this.urlHelper.userProfile);
            let headers = this.prepareHeader();
            let resp = await HttpHelper.getRequest<ResponseEntity<UserEntity>>(url, headers);
            return resp.body;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async getUsersByIdList(idList: string[]): Promise<ResponseEntity<UserEntity[]>> {
        try {
            this.init();
            let url = urljoin(this.urlHelper.baseurl, this.urlHelper.usersByIdList);
            let headers = this.prepareHeader();
            let resp = await HttpHelper.postRequest<ResponseEntity<UserEntity[]>>(url, idList, headers);
            return resp.body;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async getUserById(id: string): Promise<ResponseEntity<UserEntity>> {
        try {
            this.init();
            let url = urljoin(this.urlHelper.baseurl, this.urlHelper.userById, id);
            let headers = this.prepareHeader();
            let resp = await HttpHelper.getRequest<ResponseEntity<UserEntity>>(url, headers);
            return resp.body;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async searchUser(query: FilterQuery<UserEntity>): Promise<ResponseEntity<UserEntity>> {
        try {
            this.init();
            let url = urljoin(this.urlHelper.baseurl, this.urlHelper.searchUser);
            let headers = this.prepareHeader();
            let resp = await HttpHelper.postRequest<ResponseEntity<UserEntity>>(url, query, headers);
            return resp.body;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async getUsersCount(): Promise<ResponseEntity<number>> {
        try {
            this.init();
            let url = urljoin(this.urlHelper.baseurl, this.urlHelper.getUsersCount);
            let headers = this.prepareHeader();
            let resp = await HttpHelper.getRequest<ResponseEntity<number>>(url, headers);
            return resp.body;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async getParticipantsCount(eventId: string): Promise<ResponseEntity<number>> {
        try {
            this.init();
            let url = urljoin(this.urlHelper.baseurl, this.urlHelper.getParticipantsCount, eventId);
            let headers = this.prepareHeader();
            let resp = await HttpHelper.getRequest<ResponseEntity<number>>(url, headers);
            return resp.body;
        } catch (error) {
            Promise.reject(error);
        }
    }

    prepareHeader() {
        let contextVar = MetadataHelper.getContextVariable();
        return {
            "X-Access-Token": contextVar.tokenHash,
            "X-Ref-Number": contextVar.refNumber
        }
    }

}
